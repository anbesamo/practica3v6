﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Practica3v2Configuration.Controllers
{
    [ApiController]
    [Route("/api/students")]
    public class StudentsController : ControllerBase
    {
        Random r = new Random();

        private readonly IConfiguration _config;
        public StudentsController(IConfiguration config)
        {
            _config = config;
        }

        [HttpDelete]
        public Student DeletePerson([FromBody] Student p)
        {
            p.Name = "Deleted";
            p.LastName = "Deleted";
            p.Age = 0;

            return p;
        }

        [HttpGet]
        public List<Student> GetPersons()
        {
            List<Student> people = new List<Student>();
            for (int i = 0; i < 20; i++)
            {
                string[] Names = { "John", "Mikhail", "Sarah", "Luke", "David", "Camile", "Damon", "Grace", "Hope", "Luis" };
                string[] LastNames = { "Mikhaelson", "Stark", "Glaymore", "Dtuchin", "Hellvan", "Sarmiento", "Torrez", "Crist", "Evans", "Weasley" };


                Student p = new Student();
                p.Name = Names[r.Next(0, 9)];
                p.LastName = LastNames[r.Next(0, 9)];
                p.Age = r.Next(14, 27);

                if (!people.Contains(p))
                {
                    people.Add(p);
                }



            }


            string projectTitle = _config.GetSection("EnvironmentName").Value;
            string dbConnection = _config.GetConnectionString("DataSource");
            Console.Out.WriteLine($"We are in env :{projectTitle}");

            Console.Out.WriteLine($"We are connecting to :{dbConnection}");

            return people;
        }

        [HttpPost]
        //public Person CreatePerson([FromBody] string pname,[FromBody] string lname,[FromBody] int age)
        public Student CreatePerson([FromBody] string pname)
        {
            return new Student() { Name = pname, LastName = "Sarmiento", Age = 20 };
        }


        [HttpPut]
        public Student UpdatePerson([FromBody] Student p)
        {
            p.Name = "Updated-Name";
            p.LastName = "Updated=LastName";
            p.Age = 100;
            return p;
        }
    }
}
