using System;

namespace Practica3v2Configuration
{
    public class Student
    {

        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
